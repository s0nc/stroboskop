# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

[//]: # (![Stroboskop](stroboskop.gif) tkole se kao zakomentira reči)

Gif mi je šou že ornk na .... zato ga ni več. tkoj velik bl pregledn...

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://s0nc@bitbucket.org/s0nc/stroboskop.git
git status
git add -A
git commit -m 
git push
```

Naloga 6.2.3:
https://bitbucket.org/s0nc/stroboskop/commits/9c7368fda51f8abbe0f424ae59ca8ebf4539a43f

_pozabu napisat za sporočiu:_ “Priprava potrebnih JavaScript knjižnic”  
_se zgodi_

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/s0nc/stroboskop/commits/69057fb1684bab76471f29a892b847883b93608e

Naloga 6.3.2:
https://bitbucket.org/s0nc/stroboskop/commits/7e886a3921bafed648d6db749f41184fd67d63f1

Naloga 6.3.3:
https://bitbucket.org/s0nc/stroboskop/commits/d1dbc9a33da83f8eea4595a6b22ac93de41d3bd5

Naloga 6.3.4:
https://bitbucket.org/s0nc/stroboskop/commits/57b22098a2c935e56eca5132275025bb54e95e30

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/s0nc/stroboskop/commits/59f60cecac2862f113d6a86fab94b116f80f6eaf

Naloga 6.4.2:
https://bitbucket.org/s0nc/stroboskop/commits/bef127ec8377ff5a47d75b10df50235120c784ca

Naloga 6.4.3:
https://bitbucket.org/s0nc/stroboskop/commits/eaff138b86d1c721fbe4ed13430ed7c85d2ce320

Naloga 6.4.4:
https://bitbucket.org/s0nc/stroboskop/commits/4892e9623bc5481819be92d4baba9bb22a1abfff